rule genes_random_nearby_targeted:
    input:
        genes="feature_gene_protein-coding-0kb-flank_non-transposable-element.bed",
        feature="feature_TE_{pathway}_targets.bed",
    output:
        txt="genes-nearby-TEs_random-{size}_draws-{draws}_{pathway}-targets.txt"
    params:
        size=lambda wildcards: int(wildcards.size),
        draws=lambda wildcards: int(float(wildcards.draws)),
        flank=config["params"]["genes_random_nearby_targeted"]["flank (bp)"],
    run:
        genes = pd.read_csv(input.genes, sep="\t", header=None)

        feature = BedTool(input.feature)

        samples = bedtools.random_samples(genes, size=params.size, n=params.draws)

        with open(output.txt, "a") as out:
            out.write("{}\n".format(wildcards.pathway))
            for sample in samples:
                random_genes = BedTool.from_dataframe(sample).sort(stream=True)
                closest = random_genes.closest(feature, d=True, stream=True)
                nearby = closest.filter(lambda x: int(x[-1]) < params.flank)
                out.write("{}\n".format(len(nearby)))
