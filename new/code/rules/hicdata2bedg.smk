rule hicdata2bedg:
    input:
        txt="../primary/{chromosome_arm}_PCA_tab_50kb_binsize.txt"
    output:
        bedg="HiC-PCA_{chromosome_arm}.bedg"
    run:
        data = pd.read_csv(input.txt, sep="\t", header=0)
        
        data["chrom"] = data["chrom"].str.lstrip("Chr")
        data["start"] = data["start"].astype(int)
        data["end"] = data["end"].astype(int)

        data[["chrom", "start", "end", "orientedPrincp"]].to_csv(
            output.bedg, header=False, index=False, sep="\t"
        )
