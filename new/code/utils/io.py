import re

def get_expressed_genes(exp_file):
    """Extract genes and scores from Expression Angler output.
    """
    
    AGI = "^" + r"A[tT][1-5][gG]\d{5}"

    with open(exp_file) as records:
        for line in records:
            if re.search(AGI, line): 
                field = line.split("\t")
                agi = field[0].upper().strip()
                score = field[1].split("|")[0].strip()
                yield agi, score
