# statistical helper functions
from collections import OrderedDict
from itertools import permutations

import numpy as np
import pandas as pd
from scipy.stats import mannwhitneyu, ks_2samp

def additive_smoothing(x, alpha=1):
    """Additive smoothing of a count vector.
    """
    x = np.asarray(x)
    N = sum(x)
    d = len(x)
    return (x + alpha)/(N + alpha*d)

def test_mannwhitneyu(g, value=None, alternative="greater"):
    """Perform Mann-Whitney U test between subgroups of data frame.
    """
    
    keys = g.groups.keys()
    
    for left, right in permutations(keys, 2):
        record = OrderedDict()

        test = mannwhitneyu(
            g.get_group(left)[value],
            g.get_group(right)[value],
            alternative=alternative
        )

        record["sample"] = value
        record["left"] = left
        record["right"] = right
        record["test"] = alternative
        record["name"] = "Mann-Whitney U"
        record["statistic"] = test.statistic
        record["pvalue"] = test.pvalue

        yield record

def count_mapped_features(bed, col_mapped=None, sep_mapped=None):
    """Return set size of targeted and mapped features."""
    targetset_size = len(bed)
    mapped = sum(bed.iloc[:, col_mapped].dropna().str.split(sep_mapped).tolist(), [])
    mappedset_size = len(set(mapped))
    return targetset_size, mappedset_size

def dist_mapped_features(bed, col_count=None, targetset_size=None):
    """Return count of target features for each number of mapped targets."""
    dist = bed.iloc[:, col_count].value_counts(sort=False).reset_index()
    dist.columns = ["count", "frequency"]
    dist = dist.append([{"count": 0, "frequency": targetset_size - len(bed)}])
    return dist.sort_values(by="count")

def emp_pvalue(random, observed, greater=True):
    """Empirical p-value for testing observed value against random vector.
    """
    try:
        if greater:
            return sum(random >= observed) / len(random)
        else:
            return sum(random <= observed) / len(random)
    except ZeroDivisionError as e:
        raise ValueError("Invalid random vector") from e
    except TypeError as e:
        raise ValueError("Invalid comparison") from e
