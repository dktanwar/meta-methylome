# Genomic signals

Genomic signals are bed, bedgraph, or bigwig files where a feature or numerical
score is associated with a single nucleotide position.