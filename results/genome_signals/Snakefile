"""Genome Signals
"""

rule scan_div:
    """Scan genomes for divergent sites with shannon.

    The shannon tool takes exactly the samples given in the metadata table and
    computes the within-group JSD.
    """
    input:
        "tables/metadata/metadata_{ctx}.{etype}.{gtype}.{src}.csv"
    output:
        temp("shannon/divchr{chrom}_{ctx}_{etype}_{gtype}_{src}.bed")
    shell:
        "shannon div -m {input} -o {output}"
        " -s {wildcards.chrom} -c 5 6 -n 5mC C --chunk 100000"

rule concat_div:
    """Merge data from each chromosome in correct order.
    """
    input:
        ["shannon/divchr{chrom}_{{ctx}}_{{etype}}_{{gtype}}_{{src}}.bed".format(chrom=c)
         for c in CHROM['nuclear']]
    output:
        temp("shannon/div_{ctx}_{etype}_{gtype}_{src}.bed")
    shell:
        "awk 'FNR==1 && NR!=1{{next;}}{{print}}' {input} > {output}"

### Get BigWig from BED files
###
### Some tools like Gviz and deepTools need BigWig files to show scalar features
### over genomic regions. BigWig files are also suitable for genome browsers.

rule extract_jsd_bedgraph:
    """Extract JS divergence bedgraph.
    """
    input:
        "shannon/div_{ctx}_{etype}_{gtype}_{src}.bed"
    output:
        temp("genome_signals/jsd_{ctx}_{etype}_{gtype}_{src}.bedgraph")
    shell:
        "awk '{{OFS=\"\\t\"}} NR>1 {{ print $1,$2,$3,$4 }}' {input} > {output}"

rule extract_met_bedgraph:
    """Extract weighted methylation level bedgraph.
    """
    input:
        "shannon/div_{ctx}_{etype}_{gtype}_{src}.bed"
    output:
        temp("genome_signals/met_{ctx}_{etype}_{gtype}_{src}.bedgraph")
    shell:
        "awk '{{OFS=\"\\t\"}} NR>1 {{ print $1,$2,$3,$7/($7 + $8) }}' {input} > {output}"

rule bedgraph_to_bigwig:
    """Translate BedGraph to BigWig files.
    """
    input:
        bedgraph = "genome_signals/{signal}_{ctx}_{etype}_{gtype}_{src}.bedgraph",
        chrsizes = DATA['chr']
    output:
        protected("genome_signals/{signal}_{ctx}_{etype}_{gtype}_{src}.bw")
    shell:
        "bedGraphToBigWig {input.bedgraph} {input.chrsizes} {output}"

### bgzip/tabix

rule bgzip_div:
    """Compress GPFs for indexing.
    """
    input:
        "shannon/div_{ctx}_{etype}_{gtype}_{src}.bed"
    output:
        protected("shannon/div_{ctx}_{etype}_{gtype}_{src}.bed.gz")
    shell:
        "bgzip -c {input} > {output}"

rule tabix_div:
    """Index GPFs.
    """
    input:
        "shannon/div_{ctx}_{etype}_{gtype}_{src}.bed.gz"
    output:
        "shannon/div_{ctx}_{etype}_{gtype}_{src}.bed.gz.tbi"
    shell:
        "tabix -p bed -S1 {input}"

# rule cytosine_features:
#     """Get GC content, GC skew, and C distance statistics.
#     """
#     input:
#         fasta = ""     
#     output:
        
#     run:
#         records = SeqIO.parse(input.fasta, "fasta", alphabet=IUPAC.ambiguous_dna)
#         for rec in records:
#             sequence = rec.seq
#             = SeqUtils.GC(sequence)
#             = SeqUtils.GC_skew(sequence, window=100)
#             = SeqUtils.nt_search(str(sequence), context) # turn into series/array, then diff it

rule skew_bedg:
    """Compute GC/AT-skew for genome.

    Parameters:
        - size and step are following Xu et al.
    
    References:

    1. Grigoriev, A. Analyzing genomes with cumulative skew diagrams. Nucleic
       Acids Res 26, 2286–2290 (1998).
    2. Xu, W. et al. The R-loop is a common chromatin feature of the Arabidopsis
       genome. Nature Plants 1 (2017). doi:10.1038/s41477-017-0004-x
    """
    input:
        fasta = DATA['ass']
    output:
        gc = "genome_signals/skewGC_TAIR10.bedg",
        at = "genome_signals/skewAT_TAIR10.bedg"
    params:
        size = SKEW['window_size(bp)'],
        step = SKEW['window_step(bp)']
    run:
        records = SeqIO.parse(input.fasta, "fasta", alphabet=IUPAC.ambiguous_dna)
        with open(output.gc, 'w') as gc, open(output.at, 'w') as at:
            for rec in records:
                if rec.id in CHROM['nuclear']:
                    window = chunker(rec.seq, size=params.size, step=params.step)
                    for start, end, seq in window:
                        skewGC, skewAT = skew(seq)
                        s = str(start)
                        e = str(end)
                        gc.write('\t'.join([rec.id, s, e, str(skewGC)]) + '\n')
                        at.write('\t'.join([rec.id, s, e, str(skewAT)]) + '\n')

rule skew_bw:
    input:
        bedg = "genome_signals/skew{bp}_TAIR10.bedg",
        size = DATA['chr']
    output:
        bedg = temp("genome_signals/nonoverlapping_skew{bp}_TAIR10.bedg"),
        bigw = "genome_signals/skew{bp}_TAIR10.bw"
    shell:
        "awk 'BEGIN {{OFS=\"\t\";}} {{print $1, $2, $2 + 1, $4;}}' {input.bedg} > {output.bedg};"
        "bedGraphToBigWig {output.bedg} {input.size} {output.bigw}"

rule cytosine_coverage:
    """Count number of cytosine sites in 50kb windows.

    This is not the number of all Cs in the reference genome, but the number of
    all Cs that are covered across the population sample.
    """
    input:
        windows = "genome_regions/genomebin_50000bp.bed",
        feature = "genome_signals/shannon/div_{ctx}_{etype}_{gtype}_{src}.bed.gz",        
    output:
        bedg = "genome_signals/coverage50kb_cyt_{ctx}_{etype}_{gtype}_{src}.bedg"        
    shell:
        "bedtools coverage -sorted -counts -a {input.windows} -b {input.feature}"
        # " | awk 'BEGIN {{OFS=\"\t\";}} {{print $1, $2, $3, 1000 * $7;}}'"
        " > {output.bedg}"        

rule cytotype_coverage:
    """Count number of cytotype sites in 50kb windows.

    This is the number of Cs that have been classified into MMCs, HMCs, and MSCs
    (metastable Cs).
    """
    input:
        windows = "genome_regions/genomebin_50000bp.bed",
        feature = "genome_regions/{ctype}-lb{lb}ub{ub}_{ctx}_{etype}_{gtype}_{src}.bed",
    output:
        bedg = "genome_signals/coverage50kb_{ctype}-lb{lb}ub{ub}_{ctx}_{etype}_{gtype}_{src}.bedg"
    shell:
        "bedtools coverage -sorted -counts -a {input.windows} -b {input.feature}"
        # " | awk 'BEGIN {{OFS=\"\t\";}} {{print $1, $2, $3, 1000 * $7;}}'"
        " > {output.bedg}"

rule cytotype_reldensity:
    """Relative density of cytotype sites in 50kb windows.

    This is the number of classified Cs relative to the number of all Cs that
    are found across the population sample. This is done since different
    subgroups of data (here those that are defined by different etype, gtype,
    src) have a different coverage of Cs over the population. Thus, the density
    corrected for coverage in the subgroup is an estimator of the probability to
    find the particular cytotype in the window. It varies between 0 (=cytotype
    absent) and 1 (=all covered Cs belong to the cytotype).
    """
    input:
        cytotype = "genome_signals/coverage50kb_{ctype}-lb{lb}ub{ub}_{ctx}_{etype}_{gtype}_{src}.bedg",
        allcyt = "genome_signals/coverage50kb_cyt_{ctx}_{etype}_{gtype}_{src}.bedg",
        size = DATA['chr']
    output:
        bedg = "genome_signals/density50kb_{ctype}-lb{lb}ub{ub}_{ctx}_{etype}_{gtype}_{src}.bedg",
        bigw = "genome_signals/density50kb_{ctype}-lb{lb}ub{ub}_{ctx}_{etype}_{gtype}_{src}.bw"
    shell:
        "paste {input.allcyt} {input.cytotype}"
        " | awk 'BEGIN {{OFS=\"\t\";}} {{if ($4==0) {{d=0}} else {{d=$8/$4}}; print $1,$2,$3,d;}}'"
        " > {output.bedg};"
        "bedGraphToBigWig {output.bedg} {input.size} {output.bigw}"
