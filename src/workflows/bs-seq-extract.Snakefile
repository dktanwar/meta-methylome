configfile:
    "config.yaml"

GENOME = config["genome"]
CONTEXT = ["CpG", "CHG", "CHH"]
MDIR = "mtable/"

rule end:
    input:
        expand(MDIR + "{sample}_{context}.bedGraph.gz",
               sample=config["samples"], context=CONTEXT),
        expand(MDIR + "{sample}_{context}.bedGraph.gz.tbi",
               sample=config["samples"], context=CONTEXT),
        expand("dmc/Chr{contig}_{context}.bed.gz", contig=config["contigs"], context=CONTEXT)
        # expand("mapped_to_gff/chr{contig}_{context}.bed", contig=config["contigs"], context=CONTEXT),
        # expand("mapped_to_gff/chr{contig}_{context}.lnc_RNA.bed6", contig=config["contigs"], context=CONTEXT),
        # expand("mapped_to_gff/chr{contig}_{context}.antisense_lncRNA.bed6", contig=config["contigs"], context=CONTEXT),
        # expand("mapped_to_gff/chr{contig}_{context}.transposable_element.bed6", contig=config["contigs"], context=CONTEXT),
        # expand("mapped_to_gff/chr{contig}_{context}.gene.bed6", contig=config["contigs"], context=CONTEXT)

rule extract_mtable:
    input:
        bam = "alignments/{sample}.dupMarked.sorted.bam",
        gen = GENOME
    output:
        temp(MDIR + "{sample}.unsorted_CpG.bedGraph"),
        temp(MDIR + "{sample}.unsorted_CHG.bedGraph"),
        temp(MDIR + "{sample}.unsorted_CHH.bedGraph")
    params:
        pref = MDIR + "{sample}.unsorted"
    shell:
        "PileOMeth extract --CHG --CHH -o {params.pref}"
        " {input.gen} {input.bam}"

rule bgzip_mtable:
    input:
        MDIR + "{sample}.unsorted_{context}.bedGraph"
    output:
        MDIR + "{sample}_{context}.bedGraph.gz"
    shell:
        "tail -n +2 {input} | sort -k1,1V -k2,3n | bgzip -c > {output}"

rule index_mtable:
    input:
        MDIR + "{sample}_{context}.bedGraph.gz"
    output:
        MDIR + "{sample}_{context}.bedGraph.gz.tbi"
    shell:
        "tabix -0 -s1 -b2 -e3 -S1 {input}"

rule shannon_dmc:
    input:
        idx = expand(MDIR + "{sample}_{{context}}.bedGraph.gz.tbi",
                     sample=config["samples"]),
        dat = expand(MDIR + "{sample}_{{context}}.bedGraph.gz",
                     sample=config["samples"])
    output:
        "dmc/Chr{contig}_{context}.bed"
    shell:
        "shannon -o {output} {wildcards.contig} {input.dat}"

rule bgzip_dmc:
    input:
        "dmc/Chr{contig}_{context}.bed"
    output:
        "dmc/Chr{contig}_{context}.bed.gz"
    shell:
        "bgzip {input}"

rule bedtools_map:
    input:
        anno = config["annotation"]["araport11"],
        bed = "dmc/{chr}_{ctx}.bed"
    output:
        "mapped_to_gff/{chr}_{ctx}.bed"
    shell:
        "cut -f1-4 {input.bed}"
        " | bedtools map -c 4 -o collapse -null NaN -a {input.anno} -b stdin"
        " | grep -v 'NaN'"
        " | awk -v OFS='\\t' -F'\\t' '{{print $1, $4, $5, $7, $9, $3, $10}}'"
        " > {output}"

rule bedtools_intersect_lnc_RNA:
    input:
        anno = config["annotation"]["araport11"],
        bed = "dmc/{chr}_{ctx}.bed"
    output:
        "mapped_to_gff/{chr}_{ctx}.lnc_RNA.bed6"
    shell:
        "cut -f1-4 {input.bed}"
        " | bedtools intersect -wb -a {input.anno} -b stdin"
        " | awk -v OFS='\\t' -F'\\t' '$3~/^lnc_RNA$/ {{print $1, $11, $12, $9, $13, $7}}'"
        " > {output}"

rule bedtools_intersect_antisense_lncRNA:
    input:
        anno = config["annotation"]["araport11"],
        bed = "dmc/{chr}_{ctx}.bed"
    output:
        "mapped_to_gff/{chr}_{ctx}.antisense_lncRNA.bed6"
    shell:
        "cut -f1-4 {input.bed}"
        " | bedtools intersect -wb -a {input.anno} -b stdin"
        " | awk -v OFS='\\t' -F'\\t' '$3~/^antisense_lncRNA$/ {{print $1, $11, $12, $9, $13, $7}}'"
        " > {output}"

rule bedtools_intersect_te:
    input:
        anno = config["annotation"]["araport11"],
        bed = "dmc/{chr}_{ctx}.bed"
    output:
        "mapped_to_gff/{chr}_{ctx}.transposable_element.bed6"
    shell:
        "cut -f1-4 {input.bed}"
        " | bedtools intersect -wb -a {input.anno} -b stdin"
        " | awk -v OFS='\\t' -F'\\t' '$3~/^transposable_element$/ {{print $1, $11, $12, $9, $13, $7}}'"
        " > {output}"

rule bedtools_intersect_gene:
    input:
        anno = config["annotation"]["araport11"],
        bed = "dmc/{chr}_{ctx}.bed"
    output:
        "mapped_to_gff/{chr}_{ctx}.gene.bed6"
    shell:
        "cut -f1-4 {input.bed}"
        " | bedtools intersect -wb -a {input.anno} -b stdin"
        " | awk -v OFS='\\t' -F'\\t' '$3~/^gene$/ {{print $1, $11, $12, $9, $13, $7}}'"
        " > {output}"


#cut -f1-4 testRes/dmc/chr1_CHG.sorted.bed | bedtools intersect -wb -a chr1exon.gff -b stdin | awk -v OFS="\t" -F"\t" '{print $1, $11, $12, $9, $13, $7, $3}' > chr1.exon.chg
# huge space requirements but more convenient for downstream processing because long format:
# cut -f1-4 testRes/dmc/chr1_CpG.bed | bedtools intersect -wb -a projects/meta-methylome/data/genome/annotation/Araport11_GFF3_genes_transposons.201606.v2.sorted.gff.gz -b stdin | awk -v OFS="\t" -F"\t" '{print $1, $11, $12, $9, $13, $7, $3}' > test.intersect.bed
# more efficient for space but less convenient for downstream:
# cut -f1-4 testRes/dmc/chr1_CpG.bed | bedtools map -c 4 -o collapse -a projects/meta-methylome/data/genome/annotation/Araport11_GFF3_genes_transposons.201606.v2.sorted.gff.gz -b stdin | awk -v OFS="\t" -F"\t" '{print $1, $4, $5, $7, $9, $3, $10}' > test.intersect.bed2
